/**
 * bài 1
 * input:
 *  số ngày làm 12
 *  1 ngày làm 100.000
 * 
 * bước xử lý
 * b1: tạo biến ngaylam, luong
 * b2: gán giá trị ngaylam;
 * b3: tinh luong = ngaylam.value * 100.000
 * b4: in kết quả luong ra
 * 
 * output:
 *  kết quả luong (tổng lương)
 */
var ngaylam , luong;
ngaylam = 5;
luong = ngaylam * 100000;
console.log(luong);



/**
 * bài 2
 * input:
 *  nhập 5 số: 6 9 8 4 3
 * 
 * bước xử lý
 * b1: tạo biến a, b, c, d ,e, tb;
 * b2: gán giá trị cho a, b, c, d, e;
 * b3: tính tb = (a + b + c + d + e)/5
 * b4: in kết quả tb ra console
 * 
 * output:
 *  kết quả tb (Trung Bình)
 */

var a, b, c, d, e, tb;
a = 6;
b = 9;
c = 8;
d = 4;
e = 3;
tb = (a + b + c + d + e)/5;
console.log("Trung bình ", tb);

/**
 * bài 3
 * input:
 *  1 USD = 23.500 VND
 *  nhập 12 USD
 * 
 * bước xử lý
 * b1: tạo biến soUSD tongVND;
 * b2: gán giá trị cho soUSD;
 * b3: tính tongVND = soUSD * 235000;
 * b4: in kết quả tongVND ra console
 * 
 * output:
 *  kết quả tongVND (số tiên VND)
 */


var soUSD, tongVND;
soUSD = 12;
tongVND = soUSD * 23500;
console.log("tổng VND", tongVND);

/**
 * bài 4
 * input:
 *  cạnh dài = 8
 *  cạnh rộng = 6
 * 
 * bước xử lý
 * b1: tạo biến dai, rong, chuvi, dien tich
 * b2: gán giá trị dai, rong
 * b3: tính chuvi = (dai + rong) * 2
 * b3: tính dientich = dai * rong
 * b4: in kết quả chuvi ra console
 * b4: in kết quả dientich ra console
 * 
 * output:
 *  kết quả chuvi, dientich (chu vi, diện tích)
 */

var dai, rong, chuvi, dientich;
dai = 8;
rong = 6;
chuvi = (dai + rong) * 2;
dientich = dai * rong;
console.log("chu vi", chuvi);
console.log("diện tích", dientich);

/**
 * bài 5
 * input:
 *  nhập số 16
 * 
 * bước xử lý
 * b1: tạo biến n, hangchuc, donvi
 * b2: gán giá trị cho n;
 * b3: tách số hàng chục: hangchuc = n%100
 * b4: in kết quả tongVND ra console
 * 
 * output:
 *  kết quả tongVND (số tiên VND)
 */

var n, hangchuc, donvi, sum;
n = 16;
hangchuc = Math.floor(n%100/10);
donvi = n % 10;
sum = hangchuc + donvi;
console.log("sum", sum);

